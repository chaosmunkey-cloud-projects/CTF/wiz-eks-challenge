# [Registry Hunt](https://eksclustergames.com/challenge/2)

> A thing we learned during our research: always check the container registries.

###### Permissions

```json
{
    "secrets": [
        "get"
    ],
    "pods": [
        "list",
        "get"
    ]
}
```


## Process

Let's start off by seeing if we're able to list any of the secrets within the namespace. But from the looks of it, we're unable to do so.

![](./images/get-secrets.png)


Looking at the permissions listed, we are able to list and get information regarding the pods. Listing the pods shows `database-pod-2c9b3a4e` currently running.

![](./images/get-pods.png)

Describing the pod provided us with some interesting data.

![](./images/describe-pod.png)

The first of which is the container image used to provision the pod: `eksclustergames/base_ext_image`. My assumption was that this was being hosted on *dockerhub*, but a search came up with nothing meaning, if it was hosted there, it was set to private. However, there is an `imagePullSecret` set by the name of `registry-pull-secrets-780bab1d` which might give us some more clues.

![](./images/dockerhub.png)

As we have permissions to get secrets, it's possible to view it using the command `kubectl get secrets registry-pull-secrets-780bab1d -ojson`. By using this command, we are able to see the configuration in JSON format.

![](./images/get-secret-reg.png)

The bit of interest is the `data` field which contains a base64 encoded value by the name of `.dockerconfigjson`. It's possible to pull this out using the command line using `kubectl get secrets registry-pull-secrets-780bab1d -ojsonpath="{.data['\.dockerconfigjson']}"`.

This is well and good, but we still need to decode the output, which can be achieved by piping it into the `base64` binary: `kubectl get secrets registry-pull-secrets-780bab1d -ojsonpath="{.data['\.dockerconfigjson']}" | base64 -d`.

![](./images/docker-auth-base64.png)


The output of which is a string containing some authentication credentials for the *docker.io* container registry; this will also need to be extracted and decoded to get the username and password:

```bash
kubectl get secrets registry-pull-secrets-780bab1d -ojsonpath="{.data['\.dockerconfigjson']}" | base64 -d | jq '.auths["index.docker.io/v1/"].auth' | tr -d '"' | base64 -d
```

![](./images/reg-creds.png)

With the credentials in hand, we can now authenticate to the *docker.io* container registry. One of the pieces of information presented to is the `crane` utility has been pre-installed on this machine.

The first step in using `crane` in this instance is to authenticate to the container registry using the following command `crane auth login -u eksclustergames -p dckr_pat_YtncV-R85mG7m4lr45iYQj8FuCo docker.io`.

![](./images/crane-auth.png)


Now that we've authenticated, we can take a look at the container image used in the database pod. The image in question can be found in the spec of the pod, which in this case was `eksclustergames/base_ext_image`.

There are multiple methods for achieving the same outcome, but the method I used was to run the command `crane config docker.io/eksclustergames/base_ext_image`.

![](./images/crane-config.png)
